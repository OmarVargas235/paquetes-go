package mate

func Suma(xi ...int) int {

	var sum int

	for _, v := range xi {

		sum += v
	}

	return sum
}
